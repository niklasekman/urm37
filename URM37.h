#include <Arduino.h>

enum readMode {
  PWM_TRIG,
  PWM_INT,
  ANALOG_TRIG
};

class URM37 {
private:
  byte _triggerPin;
  byte _sensorPin;
  readMode _mode;
  volatile unsigned long _lastEchoTime;
  volatile unsigned long _echoTime;
  unsigned int _distance;
  void(*ISR_callback)();
  
public:
  URM37(byte triggerPin, byte sensorPin, readMode mode);
  bool init();
  bool init(void (*ISR_callback)(void));
  unsigned int getDistance_cm();
  void echoIsr();
};
