//#include <Servo.h>

#include "URM37.h"

URM37* ultra;   

unsigned int dist = 0;

void setup() {
  ultra = new URM37(3,2,PWM_INT); // triggerPin, sensorPin, mode
  ultra->init([]{ultra->echoIsr();});
  Serial.begin(9600);
}

void loop() {
  dist = ultra->getDistance_cm();
  Serial.println(dist);
  delay(100);
}
