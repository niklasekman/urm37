/** 
 *  @file   URM37.cpp
 *  @brief  Arduino library for interfacing with the DFRobot URM37 ultrasonic sensor.
With inspiration from

Measure pulse width using pin change

https://forum.arduino.cc/t/measure-pulse-width-using-pin-change-interrupt-millis/551715

Using interrupts from within a class

https://forum.arduino.cc/t/using-an-interrupt-from-a-method-inside-a-class/486521/11

 *  @author Niklas Ekman 
 *  @date   2021-11-16 
 * 
 */

#include <Arduino.h>

#include "URM37.h"

URM37::URM37(byte triggerPin, byte sensorPin, readMode mode) {
  _triggerPin = triggerPin; // Pin connected to COMP/TRIG on the URM37
  _sensorPin = sensorPin;   // Pin connected to ECHO in PWM_TRIG mode or
                            // DAC in ANALOG_TRIG mode on the URM37
  _mode = mode;             // Select either PWM_TRIG or ANALOG_TRIG
}

bool URM37::init() {
  bool success = false;  // Function returns a boolean indicating success or failure

  switch(_mode) {
    case PWM_TRIG:
      pinMode(_triggerPin, OUTPUT);
      digitalWrite(_triggerPin, HIGH);               
      pinMode(_sensorPin, INPUT);
      success = true;
      break;
    case ANALOG_TRIG:
      pinMode(_triggerPin, OUTPUT);
      digitalWrite(_triggerPin, HIGH);
      success = true;  
      break;
    default:
      success = false;
      break;
  }

  return success;
}

bool URM37::init(void (*ISR_callback)(void)) {
  pinMode(_triggerPin, OUTPUT);
  digitalWrite(_triggerPin, HIGH);               
  pinMode(_sensorPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(_sensorPin), ISR_callback, CHANGE);
}

unsigned int URM37::getDistance_cm() {
  bool success = false;
  digitalWrite(_triggerPin, LOW);
  //delayMicroseconds(5);
  digitalWrite(_triggerPin, HIGH);
  switch(_mode) {
    case PWM_TRIG:
      unsigned long pulseWidth = pulseIn(_sensorPin, LOW,50000);
      if(pulseWidth >= 50000) {
        success = false;
      } else {
        _distance = pulseWidth / 50;
        success = true;
      }
      break;
    case PWM_INT:
      success = true;
      break;
    case ANALOG_TRIG:
      _distance = analogRead(_sensorPin) * 1.1;
      success = true;
      break;
    default:
      success = false;
      break;
  }
  if(success == false) {
    //_distance = 666;
  }
  return _distance;
}

void URM37::echoIsr() {
  if(digitalRead(_sensorPin)) {
    _echoTime = micros() - _lastEchoTime;
    _distance = _echoTime / 50;
  } else {
    _lastEchoTime = micros();
  }
}
